#pragma once
#include "Entity.h"
#include "Renderer2D.h"
#include "texture.h"

struct BulletInfo {
	float x;
	float y;
	bool active;

};

class Bullet : public Entity
{
public:
	Bullet(sonarApp*);
	~Bullet();

	void Bullet::update(float deltaTime) override;

	void Bullet::draw(aie::Renderer2D* renderer) override;

protected:
	aie::Texture* m_Bullet;

	float x;
	float y;
	bool active;

	BulletInfo m_Bullets;
	float m_BulletSpeed;
	bool m_PressTrigger;
	float m_ShootTimer;

	float m_DeltaX;
	float m_DeltaY;
	float m_Degrees;

	int m_MousePositionX;
	int m_MousePositionY;
};

