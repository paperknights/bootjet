#include "UI.h"
#include "sonarApp.h"




UI::UI(sonarApp * game)
	:Entity(game)
{

	m_HUDLeftTexture = new aie::Texture("./textures/HUD/Board_left.png");
	m_BoardLeftPosX = 126.0f;
	m_BoardLeftPosY = 41.0f;

	m_HUDRightTexture = new aie::Texture("./textures/HUD/Board_right_off.png");
	m_BoardRightPosX = 1154.0f;
	m_BoardRightPosY = 41.0f;
	
	m_HUDSonarLightTexture = new aie::Texture("./textures/HUD/Sonar_light_on.png");
	m_SonarLightPosX = 1070.0f;
	m_SonarLightPosY = 33.0f;
}

UI::~UI()
{
}

void UI::update(float deltaTime)
{
}

void UI::draw(aie::Renderer2D * renderer)
{
	renderer->drawSprite(m_HUDLeftTexture, m_BoardLeftPosX, m_BoardLeftPosY, 256.0f, 82.0f, 0, 5);
	renderer->drawSprite(m_HUDRightTexture, m_BoardRightPosX, m_BoardRightPosY, 256.0f, 82.0f, 0, 5);

	if (!game->getSonar()->isSonarOnCooldown())
	{
		renderer->drawSprite(m_HUDSonarLightTexture, m_SonarLightPosX, m_SonarLightPosY, 45.0f, 45.0f, 0, 4);
	}
	
}
