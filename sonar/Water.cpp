#include "Water.h"
#include <iostream>
using std::cout;
using std::endl;




Water::Water(sonarApp* game)
	:Entity(game)
{

	m_Texture = new aie::Texture("./textures/oceanTexture1280x3600.png");
	m_ReefLeftTexture = new aie::Texture("./textures/reefTextureLeft.png");
	m_ReefRightTexture = new aie::Texture("./textures/reefTextureRight.png");
	waterSpriteTwoPos.y = 3600.0f;
	
}

Water::~Water()
{
}

void Water::update(float deltaTime)
{
	waterSpritePos.y -= deltaTime * 500;
	waterSpriteTwoPos.y -= deltaTime * 500;
	if (waterSpritePos.y <= -1800)
	{
		waterSpritePos.y += 5400;
	}
	if (waterSpriteTwoPos.y <= -1800)
	{
		waterSpriteTwoPos.y += 5400;
	}

	//cout << "Position of Y = " << waterSpritePos.y << endl;
}

void Water::draw(aie::Renderer2D * renderer)
{
	renderer->drawSprite(m_Texture, waterSpritePos.x , waterSpritePos.y, 1280.0f, 3600.f, 0.0f, 100.0f);
	renderer->drawSprite(m_Texture, waterSpriteTwoPos.x, waterSpriteTwoPos.y, 1280.0f, 3600.f, 0.0f, 100.0f);
	renderer->drawSprite(m_ReefLeftTexture, waterSpritePos.reefLeftX, waterSpritePos.y, 64.0f, 3600.f, 0.0f, 99.0f);
	renderer->drawSprite(m_ReefLeftTexture, waterSpriteTwoPos.reefLeftX, waterSpriteTwoPos.y, 64.0f, 3600.f, 0.0f, 99.0f);
	renderer->drawSprite(m_ReefRightTexture, waterSpritePos.reefRightX, waterSpritePos.y, 64.0f, 3600.f, 0.0f, 99.0f);
	renderer->drawSprite(m_ReefRightTexture, waterSpriteTwoPos.reefRightX, waterSpriteTwoPos.y, 64.0f, 3600.f, 0.0f, 99.0f);
}
