#include "sonarApp.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include "Water.h"
#include "Sonar.h"
#include "Player.h"
#include "Bullet.h"
#include "EnemyTurret.h"
#include "UI.h"
#include "Rock.h"
#include <glm/glm.hpp>

sonarApp::sonarApp() {

}

sonarApp::~sonarApp() {

}

bool sonarApp::startup() {
	
	m_2dRenderer = new aie::Renderer2D();
	m_font = new aie::Font("./font/consolas.ttf", 32);
	water = new Water(this);
	player = new Player(this);
	sonar = new Sonar(this);
	bullet = new Bullet(this);
	turrent = new EnemyTurret(this);
	ui = new UI(this);


	rocksVector.push_back(Rock(this, 96, 416));
	rocksVector.push_back(Rock(this, 160, 512));
	rocksVector.push_back(Rock(this, 192, 144));
	rocksVector.push_back(Rock(this, 320, 416));
	rocksVector.push_back(Rock(this, 416, 512));
	rocksVector.push_back(Rock(this, 512, 512));
	rocksVector.push_back(Rock(this, 512, 416));
	rocksVector.push_back(Rock(this, 128, 1040));
	rocksVector.push_back(Rock(this, 244, 1104));
	rocksVector.push_back(Rock(this, 320, 1040));
	rocksVector.push_back(Rock(this, 672, 1008));
	rocksVector.push_back(Rock(this, 768, 1072));
	rocksVector.push_back(Rock(this, 864, 1072));
	rocksVector.push_back(Rock(this, 960, 1072));
	rocksVector.push_back(Rock(this, 448, 1568));
	rocksVector.push_back(Rock(this, 544, 1520));
	rocksVector.push_back(Rock(this, 640, 1568));
	rocksVector.push_back(Rock(this, 544, 1792));
	rocksVector.push_back(Rock(this, 408, 1920));
	rocksVector.push_back(Rock(this, 192, 1352));
	rocksVector.push_back(Rock(this, 224, 2704));
	rocksVector.push_back(Rock(this, 288, 2320));
	rocksVector.push_back(Rock(this, 288, 2416));
	rocksVector.push_back(Rock(this, 320, 2608));
	rocksVector.push_back(Rock(this, 480, 2544));
	rocksVector.push_back(Rock(this, 576, 2608));
	rocksVector.push_back(Rock(this, 864, 2640));
	rocksVector.push_back(Rock(this, 960, 2544));
	rocksVector.push_back(Rock(this, 1056, 2448));
	rocksVector.push_back(Rock(this, 160, 3360));
	rocksVector.push_back(Rock(this, 256, 3264));
	rocksVector.push_back(Rock(this, 384, 3136));
	rocksVector.push_back(Rock(this, 480, 3136));
	rocksVector.push_back(Rock(this, 576, 3136));
	rocksVector.push_back(Rock(this, 768, 3232));
	rocksVector.push_back(Rock(this, 832, 3136));
	rocksVector.push_back(Rock(this, 928, 3136));
	rocksVector.push_back(Rock(this, 1056, 3328));
	rocksVector.push_back(Rock(this, 416, 3792));
	rocksVector.push_back(Rock(this, 512, 3856));
	rocksVector.push_back(Rock(this, 608, 3920));
	rocksVector.push_back(Rock(this, 768, 4080));
	rocksVector.push_back(Rock(this, 864, 3984));
	rocksVector.push_back(Rock(this, 960, 3888));
	rocksVector.push_back(Rock(this, 1056, 4016));


	return true;
}

void sonarApp::shutdown() {

	delete m_font;
	delete m_2dRenderer;
}

void sonarApp::update(float deltaTime) {

	// input example
	aie::Input* input = aie::Input::getInstance();

	// exit the application
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();


	if (player->playerAlive) {
		water->update(deltaTime);
		player->update(deltaTime);
		sonar->update(deltaTime);
		bullet->update(deltaTime);
		//turrent->update(deltaTime);
		ui->update(deltaTime);	
	}



	

	for (size_t i = 0; i < rocksVector.size(); i++)
	{
		MovementInfo playerMove = player->positionInfo();

		glm::vec2 PlayerPos(playerMove.m_positionX, playerMove.m_positionY);
		glm::vec2 RockPos(rocksVector[i].rockPosX, rocksVector[i].rockPosY);

		float distance = glm::distance(PlayerPos, RockPos);
		
		if (distance <= 64) {
			rocksVector[i].rockAlive = false;
			player->health -= 1;
		}
	}

	for (size_t i = 0; i < rocksVector.size(); i++)
	{
		rocksVector[i].update(deltaTime);
		if (rocksVector[i].rockAlive == false)
		{
			rocksVector.erase(rocksVector.begin() + i);
		}
	}

	if (player->health <= 0) {
		player->playerAlive = false;
		
	}

}

void sonarApp::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// begin drawing sprites
	m_2dRenderer->begin();

	// draw your stuff here!
	water->draw(m_2dRenderer);

	if (player->playerAlive) {
		player->draw(m_2dRenderer);
	}
		
	sonar->draw(m_2dRenderer);
	bullet->draw(m_2dRenderer);
	//turrent->draw(m_2dRenderer);
	ui->draw(m_2dRenderer);

	
	for (size_t i = 0; i < rocksVector.size(); i++)
	{
		rocksVector[i].draw(m_2dRenderer);
	}

	// output some text, uses the last used colour
	//m_2dRenderer->drawText(m_font, "Press ESC to quit", 0, 0);

	// done drawing sprites
	m_2dRenderer->end();
}

Player * sonarApp::getPlayer()
{
	return player;
}

Sonar * sonarApp::getSonar()
{
	return sonar;
}
