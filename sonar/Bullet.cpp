#include "Bullet.h"
#include "sonarApp.h"
#include "Texture.h"
#include "Entity.h"
#include "Input.h"
#include "Player.h"


Bullet::Bullet(sonarApp * game)
	:Entity(game)
{
	m_Bullet = new aie::Texture("./textures/bullet.png");
	
	m_PressTrigger = false;

	m_Bullets.active = false;
	m_Bullets.x = 0;
	m_Bullets.y = 0;
	m_ShootTimer = 0;

	m_MousePositionX = 0;
	m_MousePositionY = 0;
}


Bullet::~Bullet()
{
	delete m_Bullet;
}

void Bullet::update(float deltaTime)
{
	aie::Input* input = aie::Input::getInstance();

	Player* player = game->getPlayer();
	MovementInfo movement = player->positionInfo();

	/*
	
	if (input->isKeyDown(aie::INPUT_MOUSE_BUTTON_LEFT)) {
		m_PressTrigger = true;
		
		//Initial position
		m_Bullets.active = false;
		m_Bullets.x = movement.m_positionX + 10;
		m_Bullets.y = movement.m_positionY;

		//Initial degree
	}

	if (m_PressTrigger) {	
		m_ShootTimer += deltaTime;

	}
	*/

	//m_MousePositionX = input->getMouseX();
	//m_MousePositionY = input->getMouseY();


}

void Bullet::draw(aie::Renderer2D * renderer)
{
	Player* player = game->getPlayer();
	MovementInfo movement = player->positionInfo();

	renderer->drawSprite(m_Bullet, m_Bullets.x, m_Bullets.x, 1, 8/*, degrees*/); //m_movementInfo.m_positionY + 10

	//Heat seeking m
	renderer->drawSprite(m_Bullet, m_Bullets.x, m_Bullets.x, 1, 8); 


}
