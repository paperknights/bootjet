#include "Player.h"
#include "sonarApp.h"
#include <glm\glm.hpp>
#include "Input.h"
#include "Texture.h"
#include "Entity.h"
#include <iostream>
using std::cout;
using std::endl;




Player::Player(sonarApp * game)
	:Entity(game)
{
	m_Ship = new aie::Texture("./textures/playerBoatHull.png");

	m_Turret = new aie::Texture("./textures/playerBoatA_04.png"); //png

	m_Bullet = new aie::Texture("./textures/reefA.png"); //png

	m_font = new aie::Font("./font/consolas.ttf", 32);


	m_movementInfo.m_maxSpeed = 4; //0.5; //0
	m_movementInfo.m_maxForce = 0; //20
	m_movementInfo.m_positionX = 500;
	m_movementInfo.m_positionY = 500;
	m_movementInfo.m_accelerationX = 0;
	m_movementInfo.m_accelerationY = 0;
	m_movementInfo.m_velocityX = 0;
	m_movementInfo.m_velocityY = 0;

	bullets.x = 0;
	bullets.y = 0;
	m_BulletDegrees = 0;
	m_BulletActive = false;

}

Player::~Player()
{
	delete m_Ship;
	delete m_Turret;
	delete m_Turret;
	delete m_Bullet;
	delete m_font;
}

void Player::update(float deltaTime){
	
	aie::Input* input = aie::Input::getInstance();
	
#pragma region KeyBoard_Input

	if (input->isKeyDown(aie::INPUT_KEY_W))
	{
		m_movementInfo.m_accelerationY += m_movementInfo.m_maxSpeed * deltaTime * 1000; //7000
	}
	if (input->isKeyDown(aie::INPUT_KEY_S))
	{
		m_movementInfo.m_accelerationY -= m_movementInfo.m_maxSpeed * deltaTime * 1000; //2000
	}
	if (input->isKeyDown(aie::INPUT_KEY_A))
	{
		m_movementInfo.m_accelerationX -= m_movementInfo.m_maxSpeed * deltaTime * 1000; //8000
	}
	if (input->isKeyDown(aie::INPUT_KEY_D))
	{
		m_movementInfo.m_accelerationX += m_movementInfo.m_maxSpeed * deltaTime * 1000; //8000
	}

	
#pragma endregion

	//Direction for Turrent
	mousePositionX = input->getMouseX();
	mousePositionY = input->getMouseY();

	deltaX = m_movementInfo.m_positionX - mousePositionX;
	deltaY = m_movementInfo.m_positionY + 10 - mousePositionY;
	degrees = atan2(deltaY, deltaX);

	if (input->isMouseButtonDown(aie::INPUT_MOUSE_BUTTON_LEFT)) {

		m_BulletActive = true;

		cout << "Mouse pos X" << m_BulletDirection.x << endl;
		cout << "Mouse pos Y" << m_BulletDirection.y << endl;
		
		//Initial Position
		bullets.x = m_movementInfo.m_positionX;
		bullets.y = m_movementInfo.m_positionY + 10;

		//Initial degrees
		deltaX = mousePositionX - m_movementInfo.m_positionX;
		deltaY = mousePositionY - (m_movementInfo.m_positionY + 10);
		//m_BulletDegrees = atan2(deltaY, deltaX);
		//
		//m_DegreeVector.x = cos(m_BulletDegrees * PI / 180);
		//m_DegreeVector.y = sin(m_BulletDegrees * PI / 180);



		m_BulletDirection.x = deltaX;
		m_BulletDirection.y = deltaY;
		m_BulletDirection = glm::normalize(m_BulletDirection);

		//Bullets direction settings
		//m_BulletDirection.x = mousePositionX;
		//m_BulletDirection.y = mousePositionY;

	}
	
	if (m_BulletActive) {

		//Move towards mouse position  
		bullets.x += m_BulletDirection.x * deltaTime * 1000;
		bullets.y += m_BulletDirection.y * deltaTime * 1000;

		m_BulletLifeTime += deltaTime;	
	}
	if (m_BulletLifeTime >= 2) {
		m_BulletLifeTime = 0;
		m_BulletActive = false;
	}
	







#pragma region Player_Movement

	m_movementInfo.m_velocityX += m_movementInfo.m_accelerationX * deltaTime;
	m_movementInfo.m_velocityY += m_movementInfo.m_accelerationY * deltaTime;

	glm::vec2 vel(m_movementInfo.m_velocityX, m_movementInfo.m_velocityY);

	if (glm::length(vel) > m_movementInfo.m_maxSpeed) {
		vel = glm::normalize(vel) * m_movementInfo.m_maxSpeed;
		m_movementInfo.m_velocityX = vel.x;
		m_movementInfo.m_velocityY = vel.y;
	}
	//I think it should be: * 180.0000f / PI
	m_movementInfo.m_rotation = atan2(m_movementInfo.m_velocityY, m_movementInfo.m_velocityY) /** 180.0000f / 2*/;

	m_movementInfo.m_accelerationX = 0.0f;
	m_movementInfo.m_accelerationY = 0.0f;

	m_movementInfo.m_positionX += m_movementInfo.m_velocityX;
	m_movementInfo.m_positionY += m_movementInfo.m_velocityY;
#pragma endregion

#pragma region ClampWalls
	//CLAMP POSITION OF SHIP .....TOP AND BOTTOM.....................
	if (m_movementInfo.m_positionY >= 600) { //620
		m_movementInfo.m_accelerationY -= 10; //5
	}

	if (m_movementInfo.m_positionY <= 80) {
		m_movementInfo.m_accelerationY += 8; //5
	}

	//Clamp position of left and right
	if (m_movementInfo.m_positionX >= 0 && m_movementInfo.m_positionX <= 100) {
		m_movementInfo.m_accelerationX += 10; //1
	}
	if (m_movementInfo.m_positionX >= 1150 && m_movementInfo.m_positionX <= 1400) {
		m_movementInfo.m_accelerationX -= 10; //2
	}
#pragma endregion

}

void Player::draw(aie::Renderer2D * renderer)
{

	//TORRENT TEXTURE
	renderer->drawSprite(m_Turret, m_movementInfo.m_positionX, m_movementInfo.m_positionY + 10, 30, 30, degrees);

	//PLAYER
	renderer->drawSprite(m_Ship, m_movementInfo.m_positionX, m_movementInfo.m_positionY, 68, 68, 0, 1);

	//BULLET
	if (m_BulletActive) {
		renderer->drawSprite(m_Bullet, bullets.x, bullets.y, 16, 16, m_BulletDegrees, 5);	
	}

	//renderer->drawText(m_font, m_BulletDirection.x, 0, 0);

}

MovementInfo Player::positionInfo()
{
	
	return m_movementInfo;
}
