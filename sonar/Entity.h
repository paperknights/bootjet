#pragma once
#include "Application.h"
#include "Renderer2D.h"



class sonarApp;
class aie::Renderer2D;

class Entity
{
public:
	Entity(sonarApp* game);
	virtual ~Entity();

	virtual void update(float deltaTime) = 0;
	virtual void draw(aie::Renderer2D* renderer) = 0;

protected:
	sonarApp* game;
	

};

