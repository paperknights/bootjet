#include "sonarApp.h"

int main() {
	
	auto app = new sonarApp();
	app->run("AIE", 1280, 720, false);
	delete app;

	return 0;
}