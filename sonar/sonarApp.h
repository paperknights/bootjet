#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include "Entity.h"
#include <vector>

using std::vector;

#define SCREEN_WIDTH 1280;
#define SCREEN_HEIGHT 720;


class Water;
class Player;
class Sonar;
class Bullet;
class EnemyTurret;
class UI;
class Rock;

class sonarApp : public aie::Application {
public:

	sonarApp();
	virtual ~sonarApp();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

	Player* getPlayer();
	Sonar* getSonar();

protected:

	aie::Renderer2D*	m_2dRenderer;
	aie::Font*			m_font;


	Water* water;
	Player* player;
	Sonar* sonar;
	Bullet* bullet;
	EnemyTurret* turrent;
	UI* ui;
	

	vector<Rock> rocksVector;

};