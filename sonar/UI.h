#pragma once

#include "Entity.h"
#include "Renderer2D.h"
#include "texture.h"
#include "Sonar.h"

class UI : public Entity
{
public:
	UI(sonarApp*);
	~UI();


	void UI::update(float deltaTime) override;

	void UI::draw(aie::Renderer2D* renderer) override;


protected:
	aie::Texture* m_HUDLeftTexture;
	float m_BoardLeftPosX;
	float m_BoardLeftPosY;

	aie::Texture* m_HUDRightTexture;
	float m_BoardRightPosX;
	float m_BoardRightPosY;

	aie::Texture* m_HUDSonarLightTexture;
	float m_SonarLightPosX;
	float m_SonarLightPosY;

};

