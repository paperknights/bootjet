#pragma once
#include "Entity.h"
#include "Renderer2D.h"
#include "texture.h"
//#include "Bullet.h"
//#define MAX_TURRET 10

struct Vector2 {
	float x;
	float y;
};

class EnemyTurret : public Entity
{
public:
	EnemyTurret(sonarApp*);
	~EnemyTurret();

	void EnemyTurret::update(float deltaTime) override;

	void EnemyTurret::draw(aie::Renderer2D* renderer) override;

protected:

	//Bullet m_EnemyBullet;

	aie::Texture* m_EnemyTurrentBase;
	aie::Texture* m_EnemyTurrent;
	aie::Texture* m_Bullet;
	float m_TurrentPositionX;
	float m_TurrentPositionY;

	float m_TurrentPosition2X;
	float m_TurrentPosition2Y;

	//float m_TurrentPositionX[MAX_TURRET];
	//float m_TurrentPositionY[MAX_TURRET];
	float m_DeltaX;
	float m_DeltaY;

	float m_DeltaX2;
	float m_DeltaY2;

	float m_Degrees;
	float m_Degrees2;

	Vector2 direction;
	float hypotenuse;
	float speed;
	bool m_BulletShootFlag;
	float bulletTimer;

	Vector2 m_EnemyBullet;

	
};

