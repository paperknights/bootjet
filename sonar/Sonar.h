#pragma once
#include "Entity.h"
#include "Renderer2D.h"
#include "texture.h"


class Sonar : public Entity
{
public:
	Sonar(sonarApp*);
	~Sonar();

	void Sonar::update(float deltaTime) override;

	void Sonar::draw(aie::Renderer2D* renderer) override;

	bool isSonarOnCooldown();

protected:
	aie::Texture* m_Sonar;
	
	bool SonarOnCoolDown;
	bool sonarBool;
	float sonarWidth;
	float sonarHeight;
	float sonarTimer;
	float m_CooldownTimer = 0;
};

