#include "Rock.h"





Rock::Rock(sonarApp* game, float x, float y)
	:Entity(game)
{
	rockPosX = x;
	rockPosY = y;
	rockAlive = true;
	m_Texture = new aie::Texture("./textures/rockA.png");
}

Rock::~Rock()
{
}



void Rock::update(float deltaTime)
{
	rockPosY -= deltaTime * 500;
	
}

void Rock::draw(aie::Renderer2D * renderer)
{
	renderer->drawSprite(m_Texture, rockPosX, rockPosY, 64, 64, 0, 5);
}
