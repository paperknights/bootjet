#pragma once

#include "Entity.h"
#include "Renderer2D.h"
#include "texture.h"

struct waterPosition
{
	float x = 1280 / 2;
	float y = 0.0f;
	float reefLeftX = 32.0f;
	float reefRightX = 1248.0f;

};

class Water : public Entity
{
public:
	Water(sonarApp*);
	~Water();
	
	void Water::update(float deltaTime) override;

	void Water::draw(aie::Renderer2D* renderer) override;

protected:
	aie::Texture* m_Texture;
	aie::Texture* m_ReefLeftTexture;
	aie::Texture* m_ReefRightTexture;
	waterPosition waterSpritePos;
	waterPosition waterSpriteTwoPos;
	

};

