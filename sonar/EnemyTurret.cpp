#include "EnemyTurret.h"
#include "sonarApp.h"
#include "Texture.h"
#include "Entity.h"
#include "Input.h"
#include "Player.h"
#include <cmath>
using std::sqrt;


EnemyTurret::EnemyTurret(sonarApp * game)
	: Entity(game)
{
	m_EnemyTurrentBase = new aie::Texture("./textures/turretPlatformA.png");
	m_EnemyTurrent = new aie::Texture("./textures/playerBoatA_04.png");
	m_Bullet = new aie::Texture("./textures/reefA.png");

	m_TurrentPositionX = 150;
	m_TurrentPositionY = 500;

	m_TurrentPosition2X = 1000;
	m_TurrentPosition2Y = 500;

	direction.x = 0;
	direction.y = 0;

	m_EnemyBullet.x = 150;
	m_EnemyBullet.y = 500;

	speed = 5; //0.2
	hypotenuse = 0;
	

	//m_TurrentPositionX[0] = 150;
	//m_TurrentPositionY[0] = 500;
	//m_TurrentPositionX[1] = 500;
	//m_TurrentPositionY[1] = 500;
	//m_TurrentPositionX[2] = 500;
	//m_TurrentPositionY[2] = 150;
	//m_TurrentPositionX[3] = 600;
	//m_TurrentPositionY[3] = 500;
}

EnemyTurret::~EnemyTurret()
{
	delete m_EnemyTurrentBase;
	delete m_EnemyTurrent;
	delete m_Bullet;
}

void EnemyTurret::update(float deltaTime)
{
	aie::Input* input = aie::Input::getInstance();

	Player* player = game->getPlayer();
	MovementInfo movement = player->positionInfo();
	
	//First turret
	m_DeltaX = m_TurrentPositionX - movement.m_positionX;
	m_DeltaY = m_TurrentPositionY - movement.m_positionY;
	m_Degrees = atan2(m_DeltaY, m_DeltaX);

	//Second turret
	m_DeltaX2 = m_TurrentPosition2X - movement.m_positionX;
	m_DeltaY2 = m_TurrentPosition2Y - movement.m_positionY;
	m_Degrees2 = atan2(m_DeltaY2, m_DeltaX2);

	//Bullet to follow character..........................
	direction.x = movement.m_positionX - m_EnemyBullet.x;
	direction.y = movement.m_positionY - m_EnemyBullet.y;

	//Need to place it on a timer

	//Normalize the vector
	hypotenuse = sqrt(direction.x * direction.x + direction.y * direction.y);
	direction.x /= hypotenuse;
	direction.y /= hypotenuse;

	m_EnemyBullet.x += direction.x * speed;
	m_EnemyBullet.y += direction.y * speed;




	//Loop through all turrets
	/*for (size_t i = 0; i < MAX_TURRET; i++)
	{
		m_DeltaX = m_TurrentPositionX[i] - movement.m_positionX;
		m_DeltaY = m_TurrentPositionY[i] - movement.m_positionY;
		m_Degrees = atan2(m_DeltaY, m_DeltaX);
	}*/
}

void EnemyTurret::draw(aie::Renderer2D * renderer)
{
	//Turret 1
	renderer->drawSprite(m_EnemyTurrentBase, m_TurrentPositionX, m_TurrentPositionY, 64, 64, 0, 5);
	renderer->drawSprite(m_EnemyTurrent, m_TurrentPositionX, m_TurrentPositionY, 30, 30, m_Degrees, 5);

	//Turret 2
	renderer->drawSprite(m_EnemyTurrentBase, m_TurrentPosition2X, m_TurrentPosition2Y, 64, 64, 0, 5);
	renderer->drawSprite(m_EnemyTurrent, m_TurrentPosition2X, m_TurrentPosition2Y, 30, 30, m_Degrees2, 5);

	//Bullet
	renderer->drawSprite(m_Bullet, m_EnemyBullet.x, m_EnemyBullet.y, 16, 16, m_Degrees, 5);

	//Loop and draw all turrets
	/*for (size_t i = 0; i < MAX_TURRET; i++)
	{
		renderer->drawSprite(m_EnemyTurrentBase, m_TurrentPositionX[i], m_TurrentPositionY[i], 64, 64, 0, 5); 
		renderer->drawSprite(m_EnemyTurrent, m_TurrentPositionX[i], m_TurrentPositionY[i], 30, 30, m_Degrees, 5);
	}*/
}
