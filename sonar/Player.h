#pragma once
#include "Entity.h"
#include "Renderer2D.h"
#include "texture.h"
#include <glm/glm.hpp>
#include "Font.h"
#include <math.h>

#define PI 3.14159265




struct MovementInfo
{
	float m_positionX;
	float m_positionY;

	float m_velocityX;
	float m_velocityY;

	float m_accelerationX;
	float m_accelerationY;

	float m_maxSpeed;
	float m_maxForce;

	float m_rotation;
	float m_rotationDampening;
};


class Player : public Entity
{
public:

	Player(sonarApp*);
	~Player();
	
	void Player::update(float deltaTime) override;

	void Player::draw(aie::Renderer2D* renderer) override;

	MovementInfo positionInfo();
	
	
	int health = 3;


	bool playerAlive = true;

protected:
	aie::Texture* m_Ship;
	aie::Texture* m_Turret;
	aie::Texture* m_Bullet;
	aie::Font*			m_font;
	
	float deltaX;
	float deltaY;
	float degrees;
	float m_BulletDegrees;
	float m_BulletLifeTime;
	bool m_BulletActive;

	int mousePositionX;
	int mousePositionY;



	MovementInfo m_movementInfo;

	glm::vec2 bullets;
	glm::vec2 m_BulletDirection;


	glm::vec2 m_DegreeVector;

};

