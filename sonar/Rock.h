#pragma once

#include "Entity.h"
#include "Renderer2D.h"
#include "texture.h"




class Rock : public Entity
{
public:
	Rock(sonarApp*, float x, float y);
	~Rock();

	bool rockAlive;

	void Rock::update(float deltaTime) override;

	void Rock::draw(aie::Renderer2D* renderer) override;
	float rockPosX;
	float rockPosY;
	
protected:
	aie::Texture* m_Texture;

	


};

