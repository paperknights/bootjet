#include "Sonar.h"
#include "Player.h"
#include "sonarApp.h"
#include "Texture.h"
#include "Entity.h"
#include "Input.h"



Sonar::Sonar(sonarApp * game)
	:Entity(game)
{
	//SONAR IMAGE
	m_Sonar = new aie::Texture("./textures/sonarPulseRing.png");

	sonarTimer = 0;

	sonarBool = false;
	SonarOnCoolDown = false;
}

Sonar::~Sonar()
{
	delete m_Sonar;
}

void Sonar::update(float deltaTime)
{
	aie::Input* input = aie::Input::getInstance();

	//SONAR TIMER SYSTEM......................................
	if (input->isKeyDown(aie::INPUT_KEY_Q) && !SonarOnCoolDown) {
		sonarBool = true;
		SonarOnCoolDown = true;
	}
	if (sonarBool) {
		//START TIMER
		sonarTimer += deltaTime;

		//IF LESS THAN 3 SECONDS, EXPAND IMAGE
		if (sonarTimer < 2.5) {
			sonarWidth += 3000 * deltaTime;
			sonarHeight += 3000 * deltaTime;
		}
		if (sonarTimer >= 2.5) {
			sonarBool = false;
			sonarWidth = 102.5;
			sonarHeight = 92.5;
			sonarTimer = 0;
		}
	}

	if (SonarOnCoolDown) {

		m_CooldownTimer += deltaTime;

		if (m_CooldownTimer >= 10) {
			SonarOnCoolDown = false;
			m_CooldownTimer = 0;
		}
	}
	//.........................................................
}

void Sonar::draw(aie::Renderer2D * renderer)
{
	Player* player = game->getPlayer();
	MovementInfo movement = player->positionInfo();

	if (sonarBool) {
		renderer->drawSprite(m_Sonar, movement.m_positionX, movement.m_positionY, sonarWidth, sonarHeight , 0, 5); //410, 370
	}

}

bool Sonar::isSonarOnCooldown()
{
	return SonarOnCoolDown;
}
